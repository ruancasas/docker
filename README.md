## Docker

Is an Open Source platform written in Go, which is a high-performance programming language developed within Google that facilitates the creation and administration of isolated environments

## Container

Are a form of operating system-level virtualization that allows you to run multiple "systems" on a single real operating system. These isolated systems can be, from the protection of the containers, effectively isolated and limited in use of disk, as well as RAM and CPU

Container Architecture:
- DOCKER
- LXC
- LAYER FS | CGROUPS | NAMESPACES
- LINUX KERNEL

We have at the lowest layer the Linux Kernel of the OS. Cgroups and Namespaces are responsible for isolating and controlling the resources that each container will use during its existence. We also have layer management system, Layer FS, for the construction of our images and a container control interface (it can be LXC or libcontainer from Docker itself), and in the top layer we have the Docker 

## Images

Is a specification of what the container will have when it is run. It is not an active entity (an image can not be executed), but rather passive, since it only stores the instructions and data that will be used later to create the container

## Dockerfile

Its main function is the automation process of generating images, it's a text file that docker calls Dockerfile to interpret automation instructions. Each line of the file it is a instruction, after build the image it's created a cache, where in each reconstruction of the image after changes, makes use of the cache and not reinstall all again, where it allows it to be much faster

Some instructions in the Dockerfile:

    - FROM: Specify the base image
    - MAINTAINER: Owner of archive Dockerfile
    - RUN: Is executed when a container is instantiated using the image being built  
    - EXPOSE: Liberar uma porta
    - WORKDIR: Is used to set where the command defined with CMD is to be executed
    - ADD: <local_dir> <docker_dir> # Accept files (*.zip) and decompresses files when add, allowis the file path to be a URL
    - ENV: <VAR>="<VALUE>" # Create environment variable  
    - COPY: <local_dir> <docker_dir>
    - VOLUME: Is used to enable access from your container to a directory on the host machine
    - ENTRYPOINT: Argument sets the concrete default application that is used every time a container is created using the image
    - CMD: Execute commands with group of strings

Example Apache and MySQL Dockerfile
```
FROM mysql
MAINTAINER Ruan Arcega <ruanarcega@gmail.com>
RUN apt-get update && apt-get install -y apache2
ADD app/ /var/www/html
EXPOSE 8080
CMD ["/usr/bin/apache2ctl", "-D", "FOREGROUND"]
```

## Docker Compose

It is an alternative to create containers quickly and organized, plus a docker add-on tool

Example docker-compose.yml
```
database:
  image: mysql
  volumes:
    - LOCAL_HOME/:/var/lib/mysql/
  environment:
    - MYSQL_ROOT_PASSWORD=test123
  
blog:
  image: wordpress
  links:
    - database
  ports:
    - 80:80
```

## Command lines 

Clean images/containers/volumes unusable
- docker system prune

[Container]

- Inspect container:
  - docker inspect <container_id|container_name> 

- Link containers:
  - docker run --link <container_name>:<image_name> -it <container_name> bash

- Run container with environment variable:
  - docker run --name mysql -e MYSQL_ROOT_PASSWORD=teste123 -d mysql

- Run process into container in foreground:
  - docker run -d -p <local_port>:<docker_port> <container_id|container_name> /usr/sbin/apache2ctl -D FOREGROUND

- Enter into container on interative mode:
  - docker exec -i -t <container_name> bash

- Returns a command line value sent:
  - docker exec -i -t <container_name> ps aux

- Port mapping
  - docker run -it -p <local_port>:<docker_port> <container_name> bash 

- Create volume inside of container:
  - docker run -it -v $(pwd):/tmp <container_id|container_name> <instruction>

- Start container:
  - docker start <container_name>

- Stop running container:
  - docker stop <container_name>

- Kill container:
  - docker kill <container_name|container_id>

- Remove container:
  - docker rm <container_id|container_name>

- Force remove container:
  - docker rm <container_id|container_name> -f

- Remove all containers:
  - docker rm $(docker ps -qa)

[Image]

- Login in https://registry.hub.docker.com
  - docker login

- Download images:
  - docker pull <image_name>

- Find images:
  - docker search <image_name>

- Send images:
  - docker push <image_name>

- Build image:
  - docker build -t <image_name> .

- Combine the two options (exec and run) into one
- Create image and run container in interative mode:
  - docker run -i -t <image_name> bash 

- Create image and run container in background mode:
  - docker run -d <image>

- Create a new image from a container's changes:
  - docker commit -m "Install nginx package" <container_id|container_name> nginx/my_nginx

- Disposable container
  - docker run --rm -it <image> bash 

- List all images:
  - docker images

- Remove images:
  - docker rmi <image_id>

- Remove all images:
  - docker rmi $(docker images | awk '{print $3}') -f

[Process]

- View running process:
  - docker ps

- View all process:
  - docker ps -a

[Docker Compose]

- Create containers with docker-compose.yml:
  - docker-compose up

- Create containers in background mode:
  - docker-compose up -d

- List docker compose containers:
  - docker-compose list

- Kill containers:
  - docker-compose kill

- Remove containers:
  - docker-compose rm
