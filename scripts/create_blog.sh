#!/bin/bash

function run_mysql () {
        IMAGE_NAME="mysql"
        echo "===== Configure MySQL ====="
        echo ">> Container name: "
        read CONTAINER_NAME
	echo "==============================="
        echo ">> MySQL root password: "
        read MYSQL_PASSWORD
	echo "==============================="
	echo "CONTAINER: ${CONTAINER_NAME} | MYSQL_PASSWORD: ${MYSQL_PASSWORD} | IMAGE: ${IMAGE_NAME}"

        docker run --name ${CONTAINER_NAME} -e MYSQL_ROOT_PASSWORD=${MYSQL_PASSWORD} -d ${IMAGE_NAME}
}

function run_wordpress () {
        IMAGE_NAME="wordpress"
        echo "===== Configure Wordpress ====="
        echo ">> Container name: "
        read CONTAINER_NAME
	echo "==============================="
	echo ">> Containers: "
        docker ps | grep mysql 
	echo "==============================="
	echo ">> MySQL Container name: "
	read MYSQL_CONTAINER
	echo "==============================="
	echo "CONTAINER: ${CONTAINER_NAME} | IMAGE: ${IMAGE_NAME}"

        docker run --name ${CONTAINER_NAME} -d --link ${MYSQL_CONTAINER}:mysql -p 80:80 ${IMAGE_NAME}
}

option="ok"

function main () {
while true ${option} != "ok" 
do
clear
echo "============== Create blog ==============="
echo ">> 1 - Run container MySQL AND Wordpress"
echo ">> 2 - Run container MySQL" 
echo ">> 3 - Run container Wordpress"
echo ">> 4 - Stop ALL containers"
echo ">> 5 - Remove ALL containers"
echo ">> 0 - Exit"
echo "=========================================="
echo "Option: " ; read option

case ${option} in	
	1)
		run_mysql
		run_wordpress
	;;
	2)
		run_mysql
	;;
	3)
		run_wordpress
	;;
	4)
		docker stop $(docker ps -aq)
		echo "==============================="
		echo "Stoped all containers"
		sleep 0.5
	;;
	5)
        	docker rm $(docker ps -aq)
		echo "==============================="
		echo "Removed all containers"
		sleep 0.5
	;;
	0)
		echo "Leaving.."
		sleep 0.5
		clear
		exit
;;
*)
	echo "Invalid option"
esac
done
}

main
